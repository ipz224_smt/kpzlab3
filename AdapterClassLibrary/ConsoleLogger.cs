﻿namespace AdapterClassLibrary
{
    public class ConsoleLogger : ILogger
    {
        public void Log(string message)
        {
            LogWithColor(message, ConsoleColor.Green);
        }

        public void Error(string message)
        {
            LogWithColor(message, ConsoleColor.DarkRed);
        }

        public void Warn(string message)
        {
            LogWithColor(message, ConsoleColor.DarkYellow);
        }
        public void LogWithColor(string message, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(message);
            Console.ResetColor();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ProxyClassLibrary
{
    public class SmartTextReaderLocker : ITextReader
    {
        private SmartTextReader _reader;
        private SmartTextChecker _checkerreader;
        private Regex _regex;

        public SmartTextReaderLocker(string pattern)
        {
            _reader = new SmartTextReader();
            _checkerreader = new SmartTextChecker();
            _regex = new Regex(pattern);
        }

        public char[][] ReadText(string filePath)
        {
            if (_regex.IsMatch(filePath))
            {
     
                Console.WriteLine("Access denied!");
                return null;
            }
            else
            {
                return _checkerreader.ReadText(filePath);
            }
        }
    }
}

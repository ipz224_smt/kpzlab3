﻿using ComposerClassLibrary;
using LightweightClassLibrary;
using System.Net;
using System.Text;

class Program
{
    static void Main()
    {

        string filePath = @"C:\Users\Maks\Desktop\лаби\4 семестр\Конструювання програмного забезпечення (2022-2023)\design-patterns\Lab3\text.txt";
        string text = HTMLConverter.GetTextFromFile(filePath);
        Console.WriteLine(text);
        try
        {
            var rootNode = HTMLConverter.ConvertTextToHTML(text);
            Console.WriteLine(rootNode.OuterHTML);
            long memoryUsage = CalculateMemory.CalculateMemoryUsage(rootNode);
            Console.WriteLine($"Memory Usage: {memoryUsage} ");


            var rootNode2 = HTMLConverter.ConvertTextToHTMLWithLW(text);
             memoryUsage = CalculateMemory.CalculateMemoryUsage(rootNode2);
            Console.WriteLine($"Memory Usage after: {memoryUsage} ");
        }
        catch (Exception e)
        {

            Console.WriteLine(e);
        }
   
        Console.ReadLine();
    }
 



}
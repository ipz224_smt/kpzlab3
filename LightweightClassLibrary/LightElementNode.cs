﻿using ComposerClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightweightClassLibrary
{
    public class LightElementNode : ComposerClassLibrary.LightElementNode
    {
        private static Dictionary<string, LightElementNode> _flyweightPool = new Dictionary<string, LightElementNode>();

        private LightElementNode(string tagName, string displayType, string closingType, List<string> cssClasses, List<LightNode> childNodes)
            :base(tagName,displayType,closingType,cssClasses,childNodes)
        {
           
        }

        public static LightElementNode GetElementNode(string tagName, string displayType, string closingType, List<string> cssClasses, List<LightNode> childNodes)
        {
            string key = $"{tagName}_{displayType}_{closingType}_{string.Join(",", cssClasses)}";

            if (!_flyweightPool.ContainsKey(key))
            {
                _flyweightPool[key] = new LightElementNode(tagName, displayType, closingType, cssClasses, childNodes);
            }

            return _flyweightPool[key];
        }

     
    }

}

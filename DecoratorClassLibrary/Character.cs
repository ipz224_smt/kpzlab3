﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DecoratorClassLibrary
{
    public abstract class Character
    {
        public  string Name { get; set; }
        public Inventory Inventory { get; set; }
        public Character(string name, Inventory inventory)
        {
            Name = name;
            Inventory = inventory;
        }

        public virtual void Attack()
        {
            Console.WriteLine($"Attack {Name}!");
        }
    }
}

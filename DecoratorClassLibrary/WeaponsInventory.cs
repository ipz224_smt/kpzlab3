﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DecoratorClassLibrary
{
    public class WeaponsInventory: Inventory
    {
        public WeaponsInventory(Inventory inventory = null) : base(inventory)
        {
        }

        public override string Equip(string text)
        {
            return base.Equip(text) + " Equipped Weapons";
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DecoratorClassLibrary
{
    public class Paladin : Character
    {
        public Paladin(string name , Inventory inventory) : base(name, inventory)
        {
        }

        public override void Attack()
        {
            base.Attack();
            Console.WriteLine($"Smite evil with righteousness!\n");
        }
    }
}

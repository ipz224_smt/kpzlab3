﻿using static System.Net.Mime.MediaTypeNames;

namespace DecoratorClassLibrary
{
    public abstract class Inventory 
    {
        protected Inventory inventory;
        public Inventory(Inventory inventory = null)
        {
            this.inventory = inventory;
        }
        public virtual string Equip(string text)
        {
            if (inventory != null)
            {
                text = inventory.Equip(text);
            }
            return text;
        }
    }
}
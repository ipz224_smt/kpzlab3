﻿using AdapterClassLibrary;

class Program
{
    static readonly string Log = " log message!";
    static readonly string Error = " error message!";
    static readonly string Warn = " warn message!";
    static readonly string path = "text.txt";
    static void Main(string[] args)
    {
        ILogger consoleLogger = new ConsoleLogger();
        consoleLogger.Log($"Console {Log}");
        consoleLogger.Error($"Console {Error}");
        consoleLogger.Warn($"Console {Warn}");

        var writer = new FileWriter(path);
        writer.WriteLine($"File {Log}");
        writer.Write($"File {Warn}");
        Console.WriteLine(File.ReadAllText(path));

        ILogger Loggerfile = new FileWriterAdapter(path);
        Loggerfile.Log(Log);
        Loggerfile.Error(Error);
        Loggerfile.Warn(Warn);
        Console.WriteLine(File.ReadAllText(path));
    }
}

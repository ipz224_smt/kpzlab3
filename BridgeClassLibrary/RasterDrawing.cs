﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BridgeClassLibrary
{
    public class RasterDrawing : IRenderer
    {
        public void RenderAs(string text)
        {
            Console.WriteLine($"Drawing {text} as pixels");
        }
    }
}
